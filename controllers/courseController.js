const Course = require("../models/course");



/*
module.exports.addCourse = (data) => {
// Uses the information from the request body to provide all the necessary information.

if (reqBody.isAdmin)// or reqBody.isAdmin === true, depende sa logic
{

let new_course = new Course ({
    name : data.course.name,
    description : data.course.description,
    price : data.course.price 
})
;

    return new_course.save().then((new_course, error) => {

// course creation unsuccessful
        if (error) {
    return false;
    // course creation succesful
                } else {
                    return {
                        message: "new course successfully created!"
                    }

                 };
        
            });
 } let message = Promise.resolve('User must me Admin to Access this.')

 return message.then((value) => {
     return value

 // you can use this to simplify code
 // Promise.resolve('User must me Admin to Access this.')
 })

                                         }
*/
//======= ACTIVITY ============
module.exports.addCourse = (data) => {
	if(data.isAdmin){
		let new_course = new Course ({
			name: data.course.name,//courseRoutes => data=>course
			description: data.course.description,
			price: data.course.price
		})

		return new_course.save().then((new_course, error) => {
			if(error){
				return error
			}

			return {
				message: 'New course successfully created!'
			}
		})
	} 

	let message = Promise.resolve('User must be Admin to Access this.')

	return message.then((value) => {
		return value

	// you can use this to simplify code
	// Promise.resolve('User must me Admin to Access this.')
	})
		
}



// ===== activity ==========

module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
    return result;
    });

}

module.exports.updateCourse = (reqParams, reqBody) => {
	// speicify the fields/properties of the document to be updated



	let updatedCourse = {
		name : reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	/*
		Syntax:

		findByIdAndUpdate(document ID, updatesToBeApplied)

	*/

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

			if (error){
				return false;
			} else {
				return {
					message: "Course updated successfully"
				}
			};
		});
};



module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		
		isActive : false // sa schema nka default = true, para inactive isActive = false
	};

		return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
			if (error) {

				return false;
			} else {
				return {
					message : "archiving Course successfully"
				}
			};
		});

};


//==========unarchive============

module.exports.unarchiveCourse = (reqParams) => {

	let updateActiveField = {
		
		isActive : true // sa schema nka default = true, para inactive isActive = false
	};

		return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
			if (error) {

				return false;
			} else {
				return {
					message : "unarchiving Course successfully"
				}
			};
		});

};















module.exports.getAllActive = () => {

	// Mini Activity
	/*
		Make an query on the course model that retrieve all the active products. 
			- Using find method.
	*/
	
	return Course.find({isActive : true}).then(result => {
		return result
	})

	

}



