const User = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/course");

module.exports.checkEmailExists = (reqBody) =>
{// User big "U" = plural
    return User.findOne({email : reqBody.email}).then(result => {

        if ((result = true) && (result !== reqBody.email) ) {//.length length ng string if may length ang string then return result
//              return true
//if duplicate 
                return{
                    message: "User not found"
                        }
                 }
        
            if (result.length > 1) {

                return {
                    message: "Duplicate User"
                } 
    
            } else if (result.length > 0){
    
                return {
                    message: "User was found"
                }
          // return false
              
                }
    })    
} // the findOne method returns a record if a  match is found


module.exports.registerUser = (reqBody) => 
{
    let newUser = new User ({// Create a variable "newUser"
// ang content dapat same sa schema
//isulat sa postnman body json
        firstName:  reqBody.firstName,
        lastName:   reqBody.lastName,
        email:      reqBody.email,
        mobileNo:   reqBody.mobileNo,
     //   password:   reqBody.password
        password:   bcrypt.hashSync(reqBody.password, 10)
                            })
//.save save to database
        return newUser.save().then
        (
                (user, error)=>
             {

                if (error) {
                    return false;
                           }
                 else {
                    return user
                      }
             }
        );
}



module.exports.detailsUser = (reqBody) => {

    return User.find({_id : reqBody.id}).then(user => {
        
      
            return user
        

        })
    }

/*  FOR RETRIEVING WITHOUT PASSWORD

    module.exports.getProfile = (data) => {

        return User.findById(data.userId).then(result => {
    result.password = "";
    
            // Returns the user information with the password as an empty string
            return result;
    
        });
    
    };

*/
    module.exports.deleteUser = (reqParams) => {
        // The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB
        // The Mongoose method "findByIdAndRemove" method looks for the document using the "_id" field
        
        return User.findByIdAndRemove(reqParams.id).then((result, err) => {
    
                return result
            
        })
    
    }


    module.exports.getAllUsers = () => {

        // The "return" statement. Returns the result of the Mongoose method
        // Then "then" method is used to wait for the Mongoose method to finish before sending the result back to the route.
    
        return User.find({}).then(result => {//to see all database content
    
        return result;//return result in Postman
        });
    
    }


// ============ USER AUTHENTICATION ===========

/*
    STEPS:

        1. Check the database if the user email exists
        2. Compare the password provided in the login form with the password stored 
        3. Generate/return a JSON web token if the user is successfully logged in and 
        return false if not
*/

    module.exports.loginUser = (reqBody) => {

        return User.findOne({email : reqBody.email}).then(
                result => {

                    if (result == null){
                        return {
                         
                            message : "Not found in our database"
                                  }
                        } else  {
          
                  //best practice "is" = boolean
            // The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result

                            const isPasswordCorrect = bcrypt.compareSync
                            (reqBody.password, result.password);
                            //reqBody.password == word
                            //result.password == nka hash
                        if (isPasswordCorrect) {
                            return {access: auth.createAccessToken(result)}
                        } else {
                            // if password doesn't match
                            return {
                                message: "Password was incorrect"
                            }
                        };
                  };
         });
    }
    

// =======ACTIVITY===========
// ========ENROLL =========
module.exports.enroll = async (data) => {
// async = to make to functions sa "data" //asynchronous = isa2
// .await 
    let isUserUpdated = await User.findById(data.userId).then(user =>{

        user.enrollments.push({courseId : data.courseId});

        return user.save().then((user, error)=>{

            if (error) {
                     return false;
            } else {
                return true;
             };
        });
    });

    // Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});

        // Saves the updated course information in the database
		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});

    // Condition that will check if the user and course documents have been updated
	// User enrollment successful
	if(isUserUpdated && isCourseUpdated){
		return {
            message : "You're Enrolled"
        }
	// User enrollment failure
	} else {
		return false;
	};

}









    
//

    //============== ACTIVITY =============
    //controller function that retrieving all the courses