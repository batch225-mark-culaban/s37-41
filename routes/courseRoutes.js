const express = require("express");
const router = express.Router();
const auth = require("../auth");


const courseController = require("../controllers/courseController");
const course = require("../models/course");

// Router for creating a course

router.post("/addCourse", auth.verify, (req, res) =>{//add auth.verify for verification @auth.js

//==== VERIFIER =====
const data = {
    course: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
}
courseController.addCourse(data).then(result => res.send(result));
})


//kung admin pwede ka buy wala na auth.verify


//====== ACTIVITY ===========
// ===== GET ALL COURSES ======
// ===== admin capstone =============
router.get("/getCourses", (req, res) => {

    // code functions
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
    //.send sends through Postman
  });



// ====== ROUTE FOR UPDATING THE COURSE ===========

router.put("/:courseId", auth.verify, (req, res) => {

  courseController.updateCourse(req.params, req.body).then(
    result => res.send(result));
  
});

// ======= ROUTES FOR ARCHIVING ============
router.put("/archive/:courseId", auth.verify, (req, res) => {
//archiveCourse(req.params, req.body)
  courseController.archiveCourse(req.params).then(
    result => res.send(result)
  );
})


//========== UNARCHIVE ==============

router.put("/unarchive/:courseId", auth.verify, (req, res) => {
  //archiveCourse(req.params, req.body)
    courseController.unarchiveCourse(req.params).then(
      result => res.send(result)
    );

  })






//========= ROUTES FOR GET ALL ACTIVE ========
// capstone = router for users in getting all courses

router.get("/getAllActive", auth.verify, (req, res) => {

	courseController.getAllActive().then(result => res.send(result));
});


/*

  PLAN FOR CAPSTONE

  Admin - manage
        - product - all or limited - active and inactive or active? ( ans both active and inactive)

  Users - buy
        - product - all or limited - active and inactive



*/
/*
	Plan for capstone: 

	Admins - Manage - (Product - All or Limited - Active and Inactive or Active ? ANs : Both HtML : isActive

	Users - Bumili - (Product - All or Limited - Active and Inactive or Active : Ans : 
*/











module.exports = router;


