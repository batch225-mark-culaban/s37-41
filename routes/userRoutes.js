
const express = require("express");
const router = express.Router();
const auth = require("../auth");



//======== Import ============
const userController = require("../controllers/userController")

// ============= ROUTES ==============



//============= Check Email ==========
router.post("/checkEmail", (req, res) => {
     userController.checkEmailExists(req.body).then(result => res.send(result));
});

// ============ Route for Registration =======

router.post("/register", (req, res) => {

     userController.registerUser(req.body).then(result => res.send(result))

});

router.post("/details", (req, res) => {
     
     const id = req.body;
     userController.detailsUser(id).then(result => res.send(result))


})


/* for retrieving without password
// Route for retrieving user details
router.post("/details", (req, res) => {

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

});






*/

router.delete("/delete", (req, res) => {
     
         userController.deleteUser(req.body).then(result => res.send(result));
     
     });
     
router.get("/getinfo", (req, res) => {

          // code functions
          userController.getAllUsers().then(resultFromController => res.send(resultFromController));
          //.send sends through Postman
        });




//================= ROUTE FOR USER AUTHENTICATION =====================

router.post("/login", (req, res) =>{

          userController.loginUser(req.body).then(result => res.send(result))

})

//================= ROUTE FOR USER VERIFICATION =====================




//========== activity =============
//============ ENROLLEES =============
// ROUTES FOR ENROLLING AN AUTHENTICATED USER

router.post("/enroll", auth.verify, (req, res) => {

     let data = {
       // user ID will be retrieved from the request header
       userId : auth.decode(req.headers.authorization).id,
       
       // Course ID will be retrieve from the request body
       courseId : req.body.courseId
     }
   
     userController.enroll(data).then(result => res.send(result));
   
   })
   
   
   




module.exports = router;


