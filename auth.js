const jwt = require("jsonwebtoken");
// User defined string data that will be used to create our JSON web tokens
// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword

/*
const myToken = process.env.SECRET

const secret = myToken //double security
*/
const secret = "CourseBookingAPI";
// secret = secret key

//=============== CREATING TOKEN ================
module.exports.createAccessToken = (user) => {

    // The data will be received from the registration form
	// When the user logs in, a token will be created with user's information
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }


    return jwt.sign(data, secret, {
    //with expiration
    // expiresIn: 60 - for session timeout, default = seconds
    })

};

//=========== TOKEN VERIFICATION ===============
/*
    - Analogy
	        Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
*/

module.exports.verify = (req, res, next) => {
            // The token is retrieved from the request header
	        // This can be provided in postman under
		    // Authorization > Bearer Token


    let token = req.headers.authorization;
    if (typeof token !== "undefined"){
        // The "slice" method takes only the token from the information sent via the request header
		// The token sent is a type of "Bearer" token which when recieved contains the word "Bearer " as a prefix to the string
		// This removes the "Bearer " prefix and obtains only the token for verification


            console.log(token);

            token = token.slice(7, token.length);//7= for "bearer" word removed
            //example: Bearer: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyf, to remove the word "bearer", use slice


            // Validate the token using the "verify" method decrypting the token using the secret code


            // Decryption is a process that transforms encrypted information into its original format
            return jwt.verify(token, secret, (err, data) => {

                if(err){
                    return res.send({auth : "failed"})
                }else {

                    // Allows the application to proceed with the next middleware function/callback function in the route
			    	// The verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function


                    next()// to proceed to next code or function
                    //next na middleware which is module.exports.decode
                }

            })
    } else {

            return res.send({auth : "failed"});
    }

}

/*
//connected to next()
/// IMPORTANT !!! CONCEPT = DECODE CUSTOMERS, DECRYPT ADMIN
module.exports.decode = (token) => {
    //code
}

*/

// ================ TOKEN DECRYPTION =================
/*

    - Analogy
        Open the gift and get the content


*/

module.exports.decode = (token) => {

    if (typeof token !== "undefined"){
       
            console.log(token);

            token = token.slice(7, token.length);

            return jwt.verify(token, secret, (err, data) => {

                if(err){
                    return null;
                } else {

                    return jwt.decode(token, {complete: true}).payload;
                    //compplete = returns additional infomation sa jwt
                }
            })
    } else {

        return null;
    };
};
