
//ACTIVITY 1ST DAY


const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
    //mga isulat sa postman body
    firstName: { 
        type: String, 
        required: [true, "First name is required!"] //required input in first name
    },
    lastName: { 
        type: String, 
        required: [true, "Last name is required!"] //required input in last name
    },
    email: { 
        type: String, 
        required: [true, "email is required!"] //required input in email
    },
    password: { 
        type: String, 
        required: [true, "password is required!"] //required input in password
    },
    isAdmin: {
        type: Boolean,
        default : false
    },
    mobileNo: {
        type: String,//string because "+" input
        required: [true, "Mobile No. is required!"] //required input in number
    },
    enrollments: [
        {
            courseId: {
                type: String,
                required: [true, "ID is required!"]
            },
            enrolledOn : {
                type: Date,
                default: new Date()//matic input present date
            },
            status: {
                type: String,
                default: "Enrolled"// payed sa capstone
            }


        }]
});
//
//important,, "User" => atlas ==="Users"
module.exports = mongoose.model("User", userSchema);